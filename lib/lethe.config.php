<?php
# +------------------------------------------------------------------------+
# | Artlantis CMS Solutions                                                |
# +------------------------------------------------------------------------+
# | Lethe Newsletter & Mailing System                                      |
# | Copyright (c) Artlantis Design Studio 2014. All rights reserved.       |
# | Version       2.0                                                      |
# | Last modified 05.10.2016                                               |
# | Email         developer@artlantis.net                                  |
# | Web           http://www.artlantis.net                                 |
# +------------------------------------------------------------------------+
define('db_host','localhost');
define('db_name','lethe_2_1');
define('db_login','root');
define('db_pass','');
define('db_set_name','utf8');
define('db_set_charset','utf8');
define('db_set_collation','utf8_general_ci');
define('db_table_pref','lethe_');
?>