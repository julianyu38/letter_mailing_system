SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `lethe_blacklist` (
`ID` bigint(15) NOT NULL,
  `OID` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `ipAddr` varchar(255) NOT NULL,
  `reasons` int(11) NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `lethe_campaigns` (
`ID` int(11) NOT NULL,
  `OID` int(11) NOT NULL,
  `UID` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `alt_details` text,
  `launch_date` datetime NOT NULL,
  `attach` varchar(255) DEFAULT NULL,
  `webOpt` tinyint(2) NOT NULL DEFAULT '0',
  `campaign_key` varchar(50) DEFAULT NULL,
  `campaign_type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0=Newsletter 1=Autoresponder',
  `campaign_pos` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0=Pending, 1=Sending, 2=Stopped, 3=Completed',
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `campaign_sender_title` varchar(255) NOT NULL,
  `campaign_reply_mail` varchar(255) NOT NULL,
  `campaign_sender_account` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `lethe_campaign_ar` (
`ID` int(11) NOT NULL,
  `OID` int(11) NOT NULL,
  `CID` int(11) NOT NULL,
  `ar_type` tinyint(2) NOT NULL COMMENT '0-After Subscription, 1-After Unsubscription, 2-Specific Date, 3-Special Date',
  `ar_time` smallint(5) NOT NULL DEFAULT '1' COMMENT 'Number as 1 minute, 1hour',
  `ar_time_type` varchar(30) NOT NULL COMMENT 'MINUTE, HOUR, DAY, MONTH, YEAR',
  `ar_end_date` datetime NOT NULL,
  `ar_week_0` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Sunday',
  `ar_week_1` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Monday',
  `ar_week_2` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Tuesday',
  `ar_week_3` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Wednesday',
  `ar_week_4` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Thursday',
  `ar_week_5` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Friday',
  `ar_week_6` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Saturday',
  `ar_end` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'On/Off'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `lethe_campaign_groups` (
`ID` int(11) NOT NULL,
  `OID` int(11) NOT NULL DEFAULT '0',
  `CID` int(11) NOT NULL DEFAULT '0',
  `GID` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `lethe_chronos` (
`ID` bigint(20) NOT NULL,
  `OID` int(11) NOT NULL,
  `CID` int(11) NOT NULL,
  `pos` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0-In Process, 1-Flag for Remove',
  `cron_command` tinytext NOT NULL,
  `launch_date` datetime NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SAID` int(11) NOT NULL DEFAULT '0' COMMENT 'Submission Account'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `lethe_organizations` (
`ID` int(11) NOT NULL,
  `orgTag` varchar(30) NOT NULL,
  `orgName` varchar(255) NOT NULL,
  `addDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `billingDate` date NOT NULL,
  `isActive` tinyint(2) NOT NULL DEFAULT '0',
  `public_key` varchar(50) NOT NULL,
  `private_key` varchar(50) NOT NULL,
  `isPrimary` tinyint(2) NOT NULL DEFAULT '0',
  `ip_addr` varchar(50) NOT NULL,
  `api_key` varchar(50) NOT NULL,
  `daily_sent` int(11) NOT NULL DEFAULT '0',
  `daily_reset` datetime NOT NULL,
  `rss_url` varchar(255) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `lethe_organization_settings` (
`ID` int(11) NOT NULL,
  `OID` int(11) NOT NULL DEFAULT '0',
  `set_key` varchar(255) NOT NULL,
  `set_val` text NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `lethe_reports` (
`ID` bigint(20) NOT NULL,
  `OID` int(11) NOT NULL,
  `CID` int(11) NOT NULL,
  `pos` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0=Click, 1=Open, 2=Bounce',
  `ipAddr` varchar(30) NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(100) NOT NULL,
  `hit_cnt` int(11) NOT NULL DEFAULT '0',
  `bounceType` varchar(50) NOT NULL DEFAULT 'unknown',
  `extra_info` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `lethe_short_codes` (
`ID` int(11) NOT NULL,
  `OID` int(11) NOT NULL DEFAULT '0',
  `code_key` varchar(255) NOT NULL,
  `code_val` varchar(255) NOT NULL,
  `isSystem` tinyint(2) NOT NULL DEFAULT '0',
  `UID` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `lethe_submission_accounts` (
`ID` int(11) NOT NULL,
  `acc_title` varchar(255) NOT NULL,
  `daily_limit` int(11) NOT NULL DEFAULT '500',
  `daily_sent` int(11) NOT NULL DEFAULT '0',
  `daily_reset` datetime NOT NULL,
  `limit_range` int(11) NOT NULL DEFAULT '1440' COMMENT 'Limit range saved as minute',
  `send_per_conn` int(11) NOT NULL DEFAULT '20',
  `standby_time` int(11) NOT NULL DEFAULT '15',
  `systemAcc` tinyint(2) NOT NULL,
  `isDebug` tinyint(2) NOT NULL,
  `isActive` tinyint(2) NOT NULL,
  `from_title` varchar(255) NOT NULL,
  `from_mail` varchar(100) NOT NULL,
  `reply_mail` varchar(100) NOT NULL,
  `test_mail` varchar(100) NOT NULL,
  `mail_type` tinyint(2) NOT NULL,
  `send_method` tinyint(2) NOT NULL,
  `mail_engine` varchar(30) NOT NULL,
  `smtp_host` varchar(100) NOT NULL,
  `smtp_port` int(5) NOT NULL,
  `smtp_user` varchar(100) NOT NULL,
  `smtp_pass` varchar(100) NOT NULL,
  `smtp_secure` tinyint(2) NOT NULL DEFAULT '0',
  `pop3_host` varchar(100) NOT NULL,
  `pop3_port` int(5) NOT NULL,
  `pop3_user` varchar(100) NOT NULL,
  `pop3_pass` varchar(100) NOT NULL,
  `pop3_secure` tinyint(2) NOT NULL DEFAULT '0',
  `imap_host` varchar(100) NOT NULL,
  `imap_port` int(5) NOT NULL,
  `imap_user` varchar(100) NOT NULL,
  `imap_pass` varchar(100) NOT NULL,
  `imap_secure` tinyint(2) NOT NULL DEFAULT '0',
  `smtp_auth` tinyint(2) NOT NULL,
  `bounce_acc` tinyint(2) NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `aws_access_key` varchar(100) DEFAULT NULL,
  `aws_secret_key` varchar(100) DEFAULT NULL,
  `account_id` varchar(50) NOT NULL,
  `dkim_active` tinyint(2) NOT NULL DEFAULT '0',
  `dkim_domain` varchar(255) DEFAULT NULL,
  `dkim_private` text,
  `dkim_selector` varchar(255) DEFAULT NULL,
  `dkim_passphrase` varchar(255) DEFAULT NULL,
  `bounce_actions` text NOT NULL,
  `mandrill_user` varchar(255) DEFAULT NULL,
  `mandrill_key` varchar(255) DEFAULT NULL,
  `sendgrid_user` varchar(100) DEFAULT NULL,
  `sendgrid_pass` varchar(100) DEFAULT '0',
  `disable_bounce` tinyint(2) DEFAULT NULL,
  `aws_region` varchar(255) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `lethe_subscribers` (
`ID` int(11) NOT NULL,
  `OID` int(11) DEFAULT NULL,
  `GID` int(11) DEFAULT NULL,
  `subscriber_name` varchar(255) DEFAULT NULL,
  `subscriber_mail` varchar(50) DEFAULT NULL,
  `subscriber_web` varchar(255) DEFAULT NULL,
  `subscriber_date` datetime DEFAULT NULL,
  `subscriber_phone` varchar(50) DEFAULT NULL,
  `subscriber_company` varchar(255) DEFAULT NULL,
  `subscriber_full_data` text,
  `subscriber_active` tinyint(2) DEFAULT NULL,
  `subscriber_verify` tinyint(2) DEFAULT NULL,
  `subscriber_key` varchar(50) DEFAULT NULL,
  `add_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_addr` varchar(20) DEFAULT NULL,
  `subscriber_verify_key` varchar(50) NOT NULL,
  `subscriber_verify_sent_interval` datetime NOT NULL,
  `local_country` varchar(30) NOT NULL DEFAULT 'N/A',
  `local_country_code` varchar(5) NOT NULL DEFAULT 'N/A',
  `local_city` varchar(30) NOT NULL DEFAULT 'N/A',
  `local_region` varchar(30) NOT NULL DEFAULT 'N/A',
  `local_region_code` varchar(5) NOT NULL DEFAULT 'N/A',
  `subscriber_tag` varchar(100) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `lethe_subscriber_groups` (
`ID` int(11) NOT NULL,
  `OID` int(11) NOT NULL,
  `UID` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `isUnsubscribe` tinyint(2) NOT NULL DEFAULT '0',
  `isUngroup` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `lethe_subscribe_forms` (
`ID` int(11) NOT NULL,
  `OID` int(11) NOT NULL,
  `form_name` varchar(255) NOT NULL,
  `form_id` varchar(50) NOT NULL,
  `form_type` tinyint(2) NOT NULL,
  `form_success_url` varchar(255) DEFAULT NULL,
  `form_success_url_text` varchar(255) DEFAULT NULL,
  `form_success_text` varchar(255) DEFAULT NULL,
  `form_success_redir` int(11) NOT NULL DEFAULT '0',
  `form_remove` tinyint(2) NOT NULL DEFAULT '0',
  `add_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `isSystem` tinyint(2) NOT NULL DEFAULT '0',
  `form_view` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0=Vertical, 1=Horizontal, 2=Table',
  `isDraft` tinyint(2) NOT NULL DEFAULT '1',
  `include_jquery` tinyint(2) NOT NULL DEFAULT '1',
  `include_jqueryui` tinyint(2) NOT NULL DEFAULT '1',
  `form_group` int(11) NOT NULL DEFAULT '0',
  `form_errors` tinytext NOT NULL,
  `subscription_stop` tinyint(2) NOT NULL DEFAULT '0',
  `UID` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `lethe_subscribe_form_fields` (
`ID` int(11) NOT NULL,
  `OID` int(11) NOT NULL,
  `FID` int(11) NOT NULL,
  `field_label` varchar(255) NOT NULL,
  `field_name` varchar(30) NOT NULL,
  `field_type` varchar(30) NOT NULL,
  `field_required` tinyint(2) NOT NULL,
  `field_pattern` varchar(255) DEFAULT NULL,
  `field_placeholder` varchar(255) DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `field_data` varchar(255) DEFAULT NULL,
  `field_static` tinyint(2) NOT NULL DEFAULT '0',
  `field_save` varchar(20) NOT NULL,
  `field_error` varchar(255) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `lethe_tasks` (
`ID` bigint(20) NOT NULL,
  `OID` int(11) NOT NULL,
  `CID` int(11) NOT NULL,
  `subscriber_mail` varchar(100) NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `subscriber_key` varchar(50) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `lethe_templates` (
`ID` int(11) NOT NULL,
  `OID` int(11) NOT NULL,
  `UID` int(11) NOT NULL,
  `temp_name` varchar(255) NOT NULL,
  `temp_contents` longtext NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `temp_prev` varchar(255) DEFAULT '',
  `temp_type` varchar(20) NOT NULL DEFAULT 'normal',
  `isSystem` tinyint(2) NOT NULL DEFAULT '0',
  `temp_id` varchar(50) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `lethe_unsubscribes` (
`ID` bigint(20) NOT NULL,
  `OID` int(11) NOT NULL,
  `CID` int(11) NOT NULL DEFAULT '0',
  `subscriber_mail` varchar(100) NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `lethe_users` (
`ID` int(11) NOT NULL,
  `OID` int(11) NOT NULL DEFAULT '0',
  `real_name` varchar(100) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `auth_mode` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0=User, 1=Admin, 2=Super Admin',
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime NOT NULL,
  `isActive` tinyint(2) NOT NULL DEFAULT '0',
  `isPrimary` tinyint(2) NOT NULL DEFAULT '0',
  `session_token` varchar(50) NOT NULL,
  `session_time` datetime NOT NULL,
  `private_key` varchar(50) NOT NULL,
  `public_key` varchar(50) NOT NULL,
  `user_spec_view` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `lethe_user_permissions` (
`ID` int(11) NOT NULL,
  `OID` int(11) NOT NULL,
  `UID` int(11) NOT NULL,
  `perm` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;


ALTER TABLE `lethe_blacklist`
 ADD PRIMARY KEY (`ID`), ADD KEY `email` (`email`);

ALTER TABLE `lethe_campaigns`
 ADD PRIMARY KEY (`ID`), ADD KEY `ID` (`ID`) USING BTREE, ADD KEY `ID_2` (`ID`,`launch_date`,`campaign_pos`);

ALTER TABLE `lethe_campaign_ar`
 ADD PRIMARY KEY (`ID`);

ALTER TABLE `lethe_campaign_groups`
 ADD PRIMARY KEY (`ID`), ADD KEY `OIDs` (`OID`,`CID`) USING BTREE;

ALTER TABLE `lethe_chronos`
 ADD PRIMARY KEY (`ID`);

ALTER TABLE `lethe_organizations`
 ADD PRIMARY KEY (`ID`);

ALTER TABLE `lethe_organization_settings`
 ADD PRIMARY KEY (`ID`);

ALTER TABLE `lethe_reports`
 ADD PRIMARY KEY (`ID`), ADD KEY `rep_CID` (`CID`);

ALTER TABLE `lethe_short_codes`
 ADD PRIMARY KEY (`ID`);

ALTER TABLE `lethe_submission_accounts`
 ADD PRIMARY KEY (`ID`);

ALTER TABLE `lethe_subscribers`
 ADD PRIMARY KEY (`ID`), ADD KEY `GID` (`GID`), ADD KEY `subscriber_mail` (`subscriber_mail`), ADD KEY `GID_2` (`GID`,`subscriber_active`,`subscriber_verify`), ADD KEY `sub_GID` (`GID`), ADD KEY `subscriber_mail_6` (`subscriber_mail`), ADD KEY `GID_subac_sub_ver` (`GID`,`subscriber_active`,`subscriber_verify`);

ALTER TABLE `lethe_subscriber_groups`
 ADD PRIMARY KEY (`ID`);

ALTER TABLE `lethe_subscribe_forms`
 ADD PRIMARY KEY (`ID`);

ALTER TABLE `lethe_subscribe_form_fields`
 ADD PRIMARY KEY (`ID`);

ALTER TABLE `lethe_tasks`
 ADD PRIMARY KEY (`ID`), ADD UNIQUE KEY `CID_subscriber_mail` (`CID`,`subscriber_mail`), ADD UNIQUE KEY `CID_key_subscriber_mail` (`CID`,`subscriber_mail`,`subscriber_key`), ADD KEY `task_OID_CID_SUB` (`OID`,`CID`,`subscriber_mail`);

ALTER TABLE `lethe_templates`
 ADD PRIMARY KEY (`ID`);

ALTER TABLE `lethe_unsubscribes`
 ADD PRIMARY KEY (`ID`), ADD UNIQUE KEY `CD_sbmail` (`CID`,`subscriber_mail`), ADD UNIQUE KEY `CID_subscriber_mail` (`CID`,`subscriber_mail`);

ALTER TABLE `lethe_users`
 ADD PRIMARY KEY (`ID`);

ALTER TABLE `lethe_user_permissions`
 ADD PRIMARY KEY (`ID`);


ALTER TABLE `lethe_blacklist`
MODIFY `ID` bigint(15) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_campaigns`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_campaign_ar`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_campaign_groups`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_chronos`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_organizations`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_organization_settings`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_reports`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_short_codes`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_submission_accounts`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_subscribers`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_subscriber_groups`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_subscribe_forms`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_subscribe_form_fields`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_tasks`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_templates`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_unsubscribes`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_users`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lethe_user_permissions`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
